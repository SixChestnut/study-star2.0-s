<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() 
	                   + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>公告信息</title>
</head>
<body>
	
	<table border=1>
	<h3>查、删</h3>
		<tr>
			<td>编号</td>
			<td>科目名</td>
			<td>年级</td>
		</tr>
		<c:forEach var="course" items="${courseList}">
		
			<tr>	
				<!--   course id==》paper        -->
				<td><a href="/StudyStar/findPaperByCourseId?course_id=${course.id}">${course.id}</a></td>
				<td>${course.courseName}</td>
				<td><a href="#">${course.grade}</a></td>
				<td><a href="#" onclick="deleteCourse(${course.id})">删除</a></td>
				<td><a href="#" onclick="updateCourse(${course.id})">修改</a></td>
			</tr>
			
		</c:forEach>
	</table>
	
	
	<!-- 添加 -->
		<h3>增</h3>
		
		<form action="addCourse.action" method="POST">
			编号<input type="text" name="id" value="2"/>
 			科目名<input type="text" name="courseName" value="语文"/>
			成绩<input type="text" name="grade" value="23"/>
			<input type="submit" value="提交"/> 
		</form> 
	<!-- 修改-->
	<h3>改</h3>
		<form action="updateCourse.action" method="POST">
			编号<input type="text" name="id" value="3"/>
 			科目名<input type="text" name="courseName" value="语文"/>
			成绩<input type="text" name="grade" value="23"/>
			<input type="submit" value="提交"/> 
		</form> 
	
		<a href="/StudyStar/findCourseByGrade?grade=六年级">六年级</a>
		
<!-- 引入js文件 -->
<!-- jQuery -->
<script src="<%=basePath%>js/jquery-1.11.3.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<%=basePath%>js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<%=basePath%>js/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<%=basePath%>js/jquery.dataTables.min.js"></script>
<script src="<%=basePath%>js/dataTables.bootstrap.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<%=basePath%>js/sb-admin-2.js"></script>
<script type="text/javascript">
	function deleteCourse(id){
		if(confirm("确定删除吗？")){
	$.post("<%=basePath%>deleteCourse.action",{"id":id},
	function(data){
		alret("删除！");
		if(data=="success"){
			alret("删除成功！");
			window.location.reload();
		}
		else{
			alert("删除客户失败！");
            window.location.reload();
		}
	}		
	);
		}
	}
</script>
</body>
</html>