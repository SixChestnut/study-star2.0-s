<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() 
	                   + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>试卷信息</title>
</head>
<body>
	
	<table border=1>
	<h3>查、删</h3>
		<c:forEach var="paper" items="${paperList}">
			<tr>
				<td>试卷编号</td>
				<td>试卷名称</td>
				<td>题目题号</td>
				<td>出题对象</td>
				<td>课程编号</td>
				<td>出题老师编号</td>
			</tr>
			<tr>
				<td>${paper.id}</td>
				<td>${paper.paper_name}</td>
				<td><a href="/StudyStar/findQuestionTypeByIdList?subject_number=${paper.subject_number}">${paper.subject_number}</a></td>
				<td>${paper.question_object}</td>
				<td>${paper.course_id}</td>
				<td>${paper.user_id}</td>
				<td><a href="#" onclick="deletePaper(${paper.id})">删除</a></td>
				<td><a href="#" onclick="updatePaper(${paper.id})">修改</a>
			</tr>
		</c:forEach>
	</table>
	
	<!-- 添加 -->
		<h3>增</h3>
		
		<form action="addPaper.action" method="POST">
			题目编号<input type="text" name="id" value="55"/>
 			题目内容<input type="text" name="paper_name" value="qq"/>
			选项<input type="text" name="subject_number" value="呃呃"/>
 			题目答案<input type="text" name="question_object" value="呃呃"/>
 			解析<input type="text" name="course_id" value="22"/>
 			题目来源<input type="text" name="user_id" value="3"/>
			<input type="submit" value="提交"/> 
		</form> 
	<!-- 修改-->
	<h3>改</h3>
		<form action="updatePaper.action" method="POST">
			题目编号<input type="text" name="id" value="55"/>
 			题目内容<input type="text" name="paper_name" value="qq"/>
			选项<input type="text" name="subject_number" value="呃呃"/>
 			题目答案<input type="text" name="question_object" value="呃呃"/>
 			解析<input type="text" name="course_id" value="22"/>
 			题目来源<input type="text" name="user_id" value="3"/>
			<input type="submit" value="提交"/> 
		</form> 
		
<!-- 引入js文件 -->
<!-- jQuery -->
<script src="<%=basePath%>js/jquery-1.11.3.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<%=basePath%>js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<%=basePath%>js/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<%=basePath%>js/jquery.dataTables.min.js"></script>
<script src="<%=basePath%>js/dataTables.bootstrap.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<%=basePath%>js/sb-admin-2.js"></script>
<script type="text/javascript">
	function deletePaper(id){
		if(confirm("确定删除吗？")){
	$.post("<%=basePath%>deletePaper.action",{"id":id},
	function(data){
		alret("删除！");
		if(data=="success"){
			alret("删除成功！");
			window.location.reload();
		}
		else{
			alert("删除客户失败！");
            window.location.reload();
		}
	}		
	);
		}
	}
</script>
</body>
</html>