<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() 
	                   + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>题型信息</title>
</head>
<body>
	
	<table border=1>
	<h3>查、删</h3>
		<c:forEach var="grade" items="${gradeList}">
			<tr>
				<td>成绩编号</td>
				<td>学生编号</td>
				<td>试卷编号</td>
				<td>分数</td>
				<td>阅卷人</td>
				<td>评语</td>
			</tr>
			<tr>
				<td>${grade.id}</td>
				<td>${grade.user_id}</td>
				<td>${grade.tp_id}</td>
				<td>${grade.score}</td>
				<td>${grade.publish}</td>
				<td>${grade.remark}</td>
				<td><a href="#" onclick="deleteGrade(${grade.id})">删除</a></td>
				<td><a href="#" onclick="updateGrade(${grade.id})">修改</a>
			</tr>
		</c:forEach>
	</table>
	
	<!-- 添加 -->
		<h3>增</h3>
		
		<form action="addGrade.action" method="POST">
			成绩编号<input type="text" name="id" value="2"/>
			学生编号<input type="text" name="user_id" value="2"/>
 			试卷编号<input type="text" name="tp_id" value="3"/>
			分数<input type="text" name="score" value="ff"/>
 			阅卷人<input type="text" name="publish" value="gg"/>
 			评语<input type="text" name="remark" value="44"/>
			<input type="submit" value="提交"/> 
		</form> 
	<!-- 修改-->
	<h3>改</h3>
		<form action="updateGrade.action" method="POST">
			成绩编号<input type="text" name="id" value="2"/>
			学生编号<input type="text" name="user_id" value="2"/>
 			试卷编号<input type="text" name="tp_id" value="3"/>
			分数<input type="text" name="score" value="ff"/>
 			阅卷人<input type="text" name="publish" value="gg"/>
 			评语<input type="text" name="remark" value="44"/>
			<input type="submit" value="提交"/> 
		</form> 
		
<!-- 引入js文件 -->
<!-- jQuery -->
<script src="<%=basePath%>js/jquery-1.11.3.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<%=basePath%>js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<%=basePath%>js/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<%=basePath%>js/jquery.dataTables.min.js"></script>
<script src="<%=basePath%>js/dataTables.bootstrap.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<%=basePath%>js/sb-admin-2.js"></script>
<script type="text/javascript">
	function deleteGrade(id){
		if(confirm("确定删除吗？")){
	$.post("<%=basePath%>deleteGrade.action",{"id":id},
	function(data){
		alret("删除！");
		if(data=="success"){
			alret("删除成功！");
			window.location.reload();
		}
		else{
			alert("删除客户失败！");
            window.location.reload();
		}
	}		
	);
		}
	}
</script>
</body>
</html>