<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() 
	                   + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>题型信息</title>
</head>
<body>
	
	<table border=1>
	<h3>查、删</h3>
		<c:forEach var="questionType" items="${questionTypeList}">
			<tr>
				<td>题型编号</td>
				<td>题目编号</td>
				<td>类型</td>
				<td>难度</td>
				<td>题目分值</td>
				<td>科目编号</td>
			</tr>
			<tr>
				<td>${questionType.id}</td>
				<td>${questionType.topic_id}</td>
				<td>${questionType.type}</td>
				<td>${questionType.difficulty}</td>
				<td>${questionType.score}</td>
				<td>${questionType.course_id}</td>
				<td><a href="#" onclick="deleteQuestionType(${questionType.id})">删除</a></td>
				<td><a href="#" onclick="updateQuestionType(${questionType.id})">修改</a>
			</tr>
		</c:forEach>
	</table>
	
	<!-- 添加 -->
		<h3>增</h3>
		
		<form action="addQuestionType.action" method="POST">
			题型编号<input type="text" name="id" value="2"/>
 			题目编号<input type="text" name="topic_id" value="3"/>
			类型<input type="text" name="type" value="ff"/>
 			难度<input type="text" name="difficulty" value="gg"/>
 			题目分值<input type="text" name="score" value="44"/>
 			科目编号<input type="text" name="course_id" value="22"/>
			<input type="submit" value="提交"/> 
		</form> 
	<!-- 修改-->
	<h3>改</h3>
		<form action="updateQuestionType.action" method="POST">
			题型编号<input type="text" name="id" value="2"/>
 			题目编号<input type="text" name="topic_id" value="44"/>
			类型<input type="text" name="type" value="呃呃"/>
 			难度<input type="text" name="difficulty" value="呃呃"/>
 			题目分值<input type="text" name="score" value="33"/>
 			科目编号<input type="text" name="course_id" value="55"/>
			<input type="submit" value="提交"/> 
		</form> 
		
<!-- 引入js文件 -->
<!-- jQuery -->
<script src="<%=basePath%>js/jquery-1.11.3.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<%=basePath%>js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<%=basePath%>js/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<%=basePath%>js/jquery.dataTables.min.js"></script>
<script src="<%=basePath%>js/dataTables.bootstrap.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<%=basePath%>js/sb-admin-2.js"></script>
<script type="text/javascript">
	function deleteQuestionType(id){
		if(confirm("确定删除吗？")){
	$.post("<%=basePath%>deleteQuestionType.action",{"id":id},
	function(data){
		alret("删除！");
		if(data=="success"){
			alret("删除成功！");
			window.location.reload();
		}
		else{
			alert("删除客户失败！");
            window.location.reload();
		}
	}		
	);
		}
	}
</script>
</body>
</html>