<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() 
	                   + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>公告信息</title>
</head>
<body>
	
	<table border=1>
	<h3>查、删</h3>
		<c:forEach var="notice" items="${noticeList}">
			<tr>
				<td>${notice.id}</td>
				<td>${notice.title}</td>
				<td>${notice.context}</td>
				<td>${notice.time}</td>
				<td>${notice.publish}</td>
				<td><a href="#" onclick="deleteNotice(${notice.id})">删除</a></td>
				<td><a href="#" onclick="updateNotice(${notice.id})">修改</a>
			</tr>
		</c:forEach>
		<tr>
			<td></td>
		
		
		<%-- 	<td><fmt:formatDate value="${notice.time}" pattern="yyyy-MM-dd HH:mm:ss"/></td> --%>
		</tr>
	</table>
	
	<!-- 添加 -->
		<h3>增</h3>
		<form action="addNotice.action" method="POST">
<!-- 			<input type="text" name="id" value="55"/>
 -->			<input type="text" name="title" value="qq"/>
			<input type="text" name="context" value="呃呃"/>
<%-- 			<input type='<fmt:formatDate value="2002-12-12 12:12:12"/>' name="time"/>
 --%>			<input type="text" name="publish" value="呃呃"/>
			<input type="submit" value="提交"/> 
		</form> 
	<!-- 修改-->
	<h3>改</h3>
		<form action="updateNotice.action" method="POST">
			<input type="text" name="id" value="2"/>
			<input type="text" name="title" value="qq去"/>
			<input type="text" name="context" value="呃呃千万"/>
<%-- 			<input type='<fmt:formatDate value="2002-12-12 12:12:12"/>' name="time"/>
 --%>			<input type="text" name="publish" value="呃呃"/>
			<input type="submit" value="提交"/> 
		</form> 
		<%-- 	<td><fmt:formatDate value="${notice.time}" pattern="yyyy-MM-dd HH:mm:ss"/></td> --%>
		</tr>
	</table>
<!-- 引入js文件 -->
<!-- jQuery -->
<script src="<%=basePath%>js/jquery-1.11.3.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<%=basePath%>js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<%=basePath%>js/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<%=basePath%>js/jquery.dataTables.min.js"></script>
<script src="<%=basePath%>js/dataTables.bootstrap.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<%=basePath%>js/sb-admin-2.js"></script>
<script type="text/javascript">
	function deleteNotice(id){
		if(confirm("确定删除该公告吗？")){
	$.post("<%=basePath%>deleteNotice.action",{"id":id},
	function(data){
		alret("删除！");
		if(data=="success"){
			alret("删除成功！");
			window.location.reload();
		}
		else{
			alert("删除客户失败！");
            window.location.reload();
		}
	}		
	);
		}
	}
</script>
</body>
</html>