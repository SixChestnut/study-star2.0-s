package com.studystar.dao;

import java.util.List;

import com.studystar.domin.QuestionType;

public interface QuestionTypeDao {
	
	public List<QuestionType> findQuestionTypeList();
	
	public QuestionType findQuestionTypeById(Integer id);
	
	public int addQuestionType(QuestionType questionType); 
	
	public int deleteQuestionType(Integer id);
	
	public int updateQuestionType(QuestionType QuestionType);
	
}
