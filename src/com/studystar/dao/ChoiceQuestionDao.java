package com.studystar.dao;

import java.util.List;

import com.studystar.domin.ChoiceQuestion;

public interface ChoiceQuestionDao {
	public List<ChoiceQuestion> findChoiceQuestionList(); 
	public ChoiceQuestion findChoiceQuestionById(Integer id);
	public int addChoiceQuestion(ChoiceQuestion choiceQuestion);
	public int deleteChoiceQuestion(Integer id);
	public int updateChoiceQuestion(ChoiceQuestion choiceQuestion);
}
