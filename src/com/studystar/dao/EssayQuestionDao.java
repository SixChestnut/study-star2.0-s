package com.studystar.dao;

import java.util.List;

import com.studystar.domin.EssayQuestion;

public interface EssayQuestionDao {
	
	public List<EssayQuestion> findEssayQuestionList();
	
	public EssayQuestion findEssayQuestionById(Integer id);
	
	public int addEssayQuestion(EssayQuestion essayQuestion);
	
	public int deleteEssayQuestion(Integer id);
	
	public int updateEssayQuestion(EssayQuestion essayQuestion);
}
