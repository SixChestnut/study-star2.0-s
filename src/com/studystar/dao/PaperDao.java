package com.studystar.dao;

import java.util.List;

import com.studystar.domin.Paper;

public interface PaperDao {
	
	public List<Paper> findPaperList();
	
	public Paper findPaperById(Integer id);
	
	public List<Paper> findPaperByCourseId(int course_id);
	
	public int addPaper(Paper paper);
	
	public int deletePaper(Integer id);
	
	public int updatePaper(Paper paper);
	
}
