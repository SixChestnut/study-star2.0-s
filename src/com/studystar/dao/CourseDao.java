package com.studystar.dao;

import java.util.List;

import com.studystar.domin.Course;

public interface CourseDao {
	
	public List<Course> findCourseList();
	
	public Course findCourseById(Integer id);
	
	public List<Course> findCourseByGrade(String grade); 
	
	public int addCourse(Course course);
	
	public int deleteCourse(Integer id);
	
	public int updateCourse(Course course);
}
