package com.studystar.dao;

import java.util.List;

import com.studystar.domin.Grade;

public interface GradeDao {
	public List<Grade> findGradeList(); 
	public Grade findGradeById(Integer  id);
	public int addGrade(Grade grade);
	public int deleteGrade(Integer id);
	public int updateGrade(Grade grade);
}
