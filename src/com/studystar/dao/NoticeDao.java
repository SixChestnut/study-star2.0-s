package com.studystar.dao;

import java.util.List;

import com.studystar.domin.Notice;

/**
 * NoticeDao接口文件
 *
 */
public interface NoticeDao {
	
	
	public List<Notice> findNoticeList();
	/**
	 * 根据id查询客户信息
	 */
	public Notice findNoticeById(Integer id);
	
	public int addNotice(Notice notice);
	
	public int deleteNotice(Integer id);
	
	public int updateNotice(Notice notice);
	
	
}
