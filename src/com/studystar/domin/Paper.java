package com.studystar.domin;

public class Paper {
	private int id;
	private String paper_name;
	private String subject_number;
	private String question_object;
	private int course_id;
	private int  user_id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPaper_name() {
		return paper_name;
	}
	public void setPaper_name(String paper_name) {
		this.paper_name = paper_name;
	}
	public String getSubject_number() {
		return subject_number;
	}
	public void setSubject_number(String subject_number) {
		this.subject_number = subject_number;
	}
	public String getQuestion_object() {
		return question_object;
	}
	public void setQuestion_object(String question_object) {
		this.question_object = question_object;
	}
	public int getCourse_id() {
		return course_id;
	}
	public void setCourse_id(int course_id) {
		this.course_id = course_id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	
}
