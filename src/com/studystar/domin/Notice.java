package com.studystar.domin;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Notice {
	private Integer id;
	private String title;
	private String context;
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")//页面写入数据库时格式化     
	/* @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")//
	 */	
	private Date time;
	 
	private String publish;
	public String getPublish() {
		return publish;
	}
	public void setPublish(String publish) {
		this.publish = publish;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	@Override
	public String toString() {
		return "Notice [id=" + id + ", title=" + title + ", context=" + context + ", time=" + time + ", publish="
				+ publish + "]";
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContext() {
		return context;
	}
	public void setContext(String context) {
		this.context = context;
	}
	public Date getTime() {
		return time;
		
	}
	public void setTime(Date time) {
		this.time = time;
	}
	
}
