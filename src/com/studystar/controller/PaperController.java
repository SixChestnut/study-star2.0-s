package com.studystar.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.studystar.domin.Paper;
import com.studystar.service.PaperService;

@Controller
public class PaperController {
	
	@Autowired
	private PaperService paperService;
	
	@RequestMapping("findAllPaper")
	public String findAllPaper(Model  model){
		List<Paper> paperList=paperService.findPaperList();
		model.addAttribute("paperList",paperList);
		return "paperTest";
		
	}
	
	@RequestMapping("/findPaperById")
	public String findPaperById(Integer id,Model model){
		Paper paper=paperService.findPaperById(id);
		model.addAttribute("paper",paper);
		return "paperTest";
	}
	
	@RequestMapping("/findPaperByCourseId")
	public String findPaperByCourseId(Integer course_id,Model model){
		List<Paper> paperList=paperService.findPaperByCourseId(course_id);
		model.addAttribute("paperList",paperList);
		return "paperTest";
	}
	
	@RequestMapping(value="/addPaper.action",method=RequestMethod.POST)
	@ResponseBody
	public String addPaper(Paper paper){
		int rows=paperService.addPaper(paper);
		if(rows>0){
	    	return "success";
	    }else{
	    	return "false";
	    }
	}
	
	@RequestMapping(value="/updatePaper.action",method=RequestMethod.POST)
	@ResponseBody
	public String updatePaper(Paper paper){
		int rows=paperService.updatePaper(paper);
		if(rows>0){
	    	return "success";
	    }else{
	    	return "false";
	    }
	}
	@RequestMapping(value="/deletePaper.action",method=RequestMethod.POST)
	@ResponseBody
	public String deletePaper(Integer id){
		int rows=paperService.deletePaper(id);
		if(rows>0){
	    	return "success";
	    }else{
	    	return "false";
	    }
	}
}
