package com.studystar.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.studystar.domin.EssayQuestion;
import com.studystar.service.EssayQuestionService;

@Controller
public class EssayQuestionController {
	@Autowired
	private EssayQuestionService essayQuestionService;
	
	@RequestMapping("findAllEssayQuestion")
	public String findAllEssayQuestion(Model model){
		List<EssayQuestion> essayQuestionList=essayQuestionService.findEssayQuestionList();
		model.addAttribute("essayQuestionList",essayQuestionList);
		return "essayQuestionTest";
	}
	
	@RequestMapping("/findEssayQuestionById")
	public String findEssayQuestionById(Integer id,Model model){
		EssayQuestion essayQuestion=essayQuestionService.findEssayQuestionById(id);
		model.addAttribute("EssayQuestion",essayQuestion);
		return "essayQuestionTest";
	}
	
	@RequestMapping(value="addEssayQuestion.action",method=RequestMethod.POST)
	@ResponseBody
	public String addEssayQuestion(EssayQuestion essayQuestion){
		
		int rows=essayQuestionService.addEssayQuestion(essayQuestion);
		if(rows>0){
	    	return "success";
	    }else{
	    	return "false";
	    }
	}
	
	@RequestMapping(value="updateEssayQuestion.action",method=RequestMethod.POST)
	@ResponseBody
	public String updateEssayQuestion(EssayQuestion essayQuestion){
		
		int rows=essayQuestionService.updateEssayQuestion(essayQuestion);
		
		if(rows>0){
			return "success";
		}else{
			return "false";
		}
	}
	
	@RequestMapping(value="deleteEssayQuestion.action",method=RequestMethod.POST)
	@ResponseBody
	public String deleteEssayQuestion(Integer id){
		int rows=essayQuestionService.deleteEssayQuestion(id);
		if(rows>0){
			return "success";
		}else{
			return "false";
		}
	}
}
