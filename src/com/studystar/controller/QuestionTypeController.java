package com.studystar.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.studystar.domin.QuestionType;
import com.studystar.service.QuestionTypeService;

@Controller
public class QuestionTypeController {
	@Autowired
	private QuestionTypeService questionTypeService;
	
	@RequestMapping("findAllQuestionType")
	public String findAllQuestionType(Model model){
		List<QuestionType> questionTypeList=questionTypeService.findQuestionTypeList();
		model.addAttribute("questionTypeList",questionTypeList);
		return "questionTypeTest";
	}
	
	@RequestMapping("/findQuestionTypeById")
	public String findQuestionTypeById(Integer id,Model model){
		QuestionType questionType=questionTypeService.findQuestionTypeById(id);
		
		model.addAttribute("questionType",questionType);
		
		return "questionTypeTest";
	}
	
	@RequestMapping("/findQuestionTypeByIdList")
	public String findQuestionTypeByIdList(String subject_number,Model model){
		List<QuestionType> questionTypeList=new ArrayList<QuestionType>();
		String[] IdList=subject_number.split("~~");
		int[]  IDList=Arrays.stream(IdList).mapToInt(Integer::parseInt).toArray();
		for(int i=0;i<IdList.length;i++){
			QuestionType questionType=questionTypeService.findQuestionTypeById(IDList[i]);
			System.out.println(IDList[i]);
			questionTypeList.add(questionType);						
		}
		model.addAttribute("questionTypeList",questionTypeList);

		return "questionTypeTest";
	}
	
	@RequestMapping(value="/addQuestionType.action",method=RequestMethod.POST)
	@ResponseBody
	public String addQuestionType(QuestionType questionType){
		int rows=questionTypeService.addQuestionType(questionType);
		if(rows>0){
	    	return "success";
	    }else{
	    	return "false";
	    }
	}
	
	@RequestMapping(value="/updateQuestionType.action",method=RequestMethod.POST)
	@ResponseBody
	public String updateQuestionType(QuestionType questionType){
		
		int rows=questionTypeService.updateQuestionType(questionType);
		
		if(rows>0){
			return "success";
		}else{
			return "false";
		}
	}
	
	@RequestMapping(value="/deleteQuestionType.action",method=RequestMethod.POST)
	@ResponseBody
	public String deleteQuestionType(Integer id){
		int rows=questionTypeService.deleteQuestionType(id);
		if(rows>0){
			return "success";
		}else{
			return "false";
		}
	}
}
