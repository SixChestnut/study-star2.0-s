package com.studystar.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.studystar.domin.ChoiceQuestion;
import com.studystar.service.ChoiceQuestionService;

@Controller
public class ChoiceQuestionController {
	@Autowired
	private ChoiceQuestionService choiceQuestionService;
	
	@RequestMapping("findAllChoiceQuestion")
	public String findAllChoiceQuestion(Model model){
		List<ChoiceQuestion> choiceQuestionList=choiceQuestionService.findChoiceQuestionList();
		model.addAttribute("choiceQuestionList",choiceQuestionList);
		return "choiceQuestionTest";
	}
	
	@RequestMapping("/findChoiceQuestionById")
	public String findChoiceQuestionById(Integer id,Model model){
		ChoiceQuestion choiceQuestion=choiceQuestionService.findChoiceQuestionById(id);
		model.addAttribute("choiceQuestion",choiceQuestion);
		
		return "choiceQuestionTest";
	}
	
	@RequestMapping(value="/addChoiceQuestion.action",method=RequestMethod.POST)
	@ResponseBody
	public String adddChoiceQuestion(ChoiceQuestion choiceQuestion){
		int rows=choiceQuestionService.addChoiceQuestion(choiceQuestion);
		 if(rows>0){
		    	return "success";
		    }else{
		    	return "false";
		    }
	}
	
	@RequestMapping(value="/updateChoiceQuestion.action",method=RequestMethod.POST)
	@ResponseBody
	public String updateChoiceQuestion(ChoiceQuestion choiceQuestion){
		int rows=choiceQuestionService.updateChoiceQuestion(choiceQuestion);
		if(rows>0){
			return "success";
		}else{
			return "false";
		}
			
	}
	
	@RequestMapping(value="/deleteChoiceQuestion.action",method=RequestMethod.POST)
	@ResponseBody
	public String deleteChoiceQuestion(Integer id){
		int rows=choiceQuestionService.deleteChoiceQuestion(id);
		if(rows>0){
			return "success";
		}else{
			return "false";
		}
	}
	
}
