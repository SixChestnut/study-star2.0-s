package com.studystar.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.studystar.domin.Grade;
import com.studystar.service.GradeService;

@Controller
public class GradeController {
	@Autowired
	private GradeService gradeService;
	
	@RequestMapping("findAllGrade")
	public String findAllGrade(Model model){
		List<Grade> gradeList=gradeService.findGradeList();
		model.addAttribute("gradeList",gradeList);
		return "gradeTest";
	}
	
	@RequestMapping("/findGradeById")
	public String findGrade(Integer id,Model model){
		Grade grade=gradeService.findGradeById(id);
		model.addAttribute("grade",grade);
		return "gradeTest";
	}
	
	@RequestMapping(value="/addGrade.action",method=RequestMethod.POST)
	@ResponseBody
	public String addGrade(Grade grade){
		
		int rows=gradeService.addGrade(grade);
		
	    if(rows>0){
	    	return "success";
	    }else{
	    	return "false";
	    }
	}
	
	@RequestMapping(value="/updateGrade.action",method=RequestMethod.POST)
	@ResponseBody
	public String updateGrade(Grade grade){
		int rows=gradeService.updateGrade(grade);
		if(rows>0){
			return "success";
		}else{
			return "false";
		}
	}
	
	@RequestMapping(value="/deleteGrade.action",method=RequestMethod.POST)
	@ResponseBody
	public String deleteGrade(int id){
		int rows=gradeService.deleteGrade(id);
		System.out.print(id+"/"+rows);
		if(rows>0){
			return "success";
		}else{
			return "false";
		}
	}
}
