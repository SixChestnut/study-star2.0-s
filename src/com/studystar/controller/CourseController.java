package com.studystar.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.studystar.domin.Course;
import com.studystar.service.CourseService;

@Controller
public class CourseController {
	@Autowired
	private CourseService courseService;
	
	@RequestMapping("findAllCourse")
	public String findAllCourse(Model model){
		List<Course> courseList=courseService.findCourseList();
		model.addAttribute("courseList",courseList);
		return "courseTest";
	}
	
	@RequestMapping("/findCourseById")
	public String findCourseById(Integer id,Model model){
		Course course=courseService.findCourseById(id);
		model.addAttribute("course",course);
		
		return "courseTest";
	}
	
	@RequestMapping("/findCourseByGrade")
	public String findCourseByGrade(String grade,Model model){
		List<Course> courseList=courseService.findCourseByGrade(grade);
		model.addAttribute("courseList", courseList);
		return "courseTest";
	}
	
	@RequestMapping(value="/addCourse.action",method=RequestMethod.POST)
	@ResponseBody
	public String addCourse(Course course){
		
		int rows=courseService.addCourse(course);
		if(rows>0){
	    	return "success";
	    }else{
	    	return "false";
	    }
	}
	
	@RequestMapping(value="/updateCourse.action",method=RequestMethod.POST)
	@ResponseBody
	public String updateCourse(Course course){
		
		int rows=courseService.updateCourse(course);
		
		if(rows>0){
			return "success";
		}else{
			return "false";
		}
	}
	
	@RequestMapping(value="deleteCourse.action",method=RequestMethod.POST)
	@ResponseBody
	public String deleteCourse(Integer id){
		int rows=courseService.deleteCourse(id);
		if(rows>0){
			return "success";
		}else{
			return "false";
		}
	}
	
}
