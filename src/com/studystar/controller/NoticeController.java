package com.studystar.controller;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.studystar.domin.Notice;
import com.studystar.service.NoticeService;

//不指定ip的话，是允许所有ip进行跨域
//@CrossOrigin
//如果是标注在控制器类上，那么允许此控制器中的所有业务方法跨域
//指定ip跨域，可配置多个，中间用逗号隔开
//@CrossOrigin(origins = "localhost",allowedHeaders = "*",
//allowCredentials = "true",methods = {RequestMethod.GET,RequestMethod.POST})
@Controller
//@CrossOrigin
public class NoticeController {
	@Autowired
	private NoticeService noticeService;
	/**
	 * all Notice
	 */
	@RequestMapping("findAllNotice")
	@ResponseBody
	//@CrossOrigin(origins="http://localhost:8080")
	public Map<String,Object> findAllNotice(){
        
		List<Notice> noticeList=noticeService.findNoticeList();
//		model.addAttribute("noticeList", noticeList);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("data", noticeList);
		System.out.println("666");
		return map;
		//return "noticeTest";
	}
	
	
	
	//查 ID
	@RequestMapping("/findNoticeById")
	public String findNoticeById(Integer id,Model model){
		Notice notice=noticeService.findNoticeById(id);
		model.addAttribute("notice", notice);
		
		return "noticeTest";
	}
	
	//增
	@RequestMapping(value="/addNotice.action",method=RequestMethod.POST)
	@ResponseBody
	public String addNotice(Notice notice){
		
		Date time=new Date();
		Timestamp timeStamp=new Timestamp(time.getTime());
		
		notice.setTime(timeStamp);
	    int rows=noticeService.addNotice(notice);
	    
	    if(rows>0){
	    	return "success";
	    }else{
	    	return "false";
	    }
	}
	
	//改
	@RequestMapping(value="/updateNotice.action",method=RequestMethod.POST)
	@ResponseBody
	public String updateNotice(Notice notice){
		
		Date time=new Date();
		Timestamp timeStamp=new Timestamp(time.getTime());
		notice.setTime(timeStamp);
		
		int rows=noticeService.updateNotice(notice);
		
		if(rows>0){
			return "success";
		}else{
			return "false";
		}
	}
	
	//删
	@RequestMapping(value="/deleteNotice.action",method=RequestMethod.POST)
	@ResponseBody
	public String deleteNotice(Integer id){
		
		int rows=noticeService.deleteNotice(id);
		System.out.println(rows);
		if(rows>0){
			return "success";
		}else{
			return "false";
		}
		
	}
}
