package com.studystar.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.studystar.dao.QuestionTypeDao;
import com.studystar.domin.QuestionType;
import com.studystar.service.QuestionTypeService;

@Service
@Transactional
public class QuestionTypeServiceImpl implements QuestionTypeService{
	@Autowired
	private QuestionTypeDao questionTypeDao;
	
	@Override
	public List<QuestionType> findQuestionTypeList(){
		return this.questionTypeDao.findQuestionTypeList();
	}
	
	@Override
	public QuestionType findQuestionTypeById(int id){
		return this.questionTypeDao.findQuestionTypeById(id);
	}
	
	@Override
	public int addQuestionType(QuestionType questionType){
		return this.questionTypeDao.addQuestionType(questionType);
	}
	
	@Override
	public int deleteQuestionType(int id){
		return this.questionTypeDao.deleteQuestionType(id);
	}
	
	@Override
	public int updateQuestionType(QuestionType questionType){
		return this.questionTypeDao.updateQuestionType(questionType);
	}
}
