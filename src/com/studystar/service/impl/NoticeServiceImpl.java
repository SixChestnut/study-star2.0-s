package com.studystar.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.studystar.dao.NoticeDao;
import com.studystar.domin.Notice;
import com.studystar.service.NoticeService;

@Service
@Transactional
public class NoticeServiceImpl implements NoticeService{
	@Autowired
	private NoticeDao noticeDao;
	
	@Override
	public List<Notice> findNoticeList(){
		return this.noticeDao.findNoticeList();
	}
	
	@Override
	public Notice findNoticeById(Integer id){
		return this.noticeDao.findNoticeById(id);
	}
	
	@Override
	public int addNotice(Notice notice){
		return this.noticeDao.addNotice(notice);
	}
	
	@Override
	public int deleteNotice(int id){
		return this.noticeDao.deleteNotice(id);
	}
	
	@Override
	public int updateNotice(Notice notice){
		return this.noticeDao.updateNotice(notice);
	}
}
