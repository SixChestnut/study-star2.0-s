package com.studystar.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.studystar.dao.PaperDao;
import com.studystar.domin.Paper;
import com.studystar.service.PaperService;

@Service
@Transactional
public class PaperServiceImpl implements PaperService{
	
	@Autowired
	private PaperDao paperDao;
	
	@Override
	public List<Paper> findPaperList(){
		return this.paperDao.findPaperList();
	}
	
	@Override
	public Paper findPaperById(Integer id){
		return this.paperDao.findPaperById(id);
	}
	
	@Override
	public List<Paper> findPaperByCourseId(int course_id){
		return this.paperDao.findPaperByCourseId(course_id);
	}
	
	@Override
	public int addPaper(Paper paper){
		return this.paperDao.addPaper(paper);
	}
	
	@Override
	public int deletePaper(int id){
		return this.paperDao.deletePaper(id);
	}
	
	@Override
	public int updatePaper(Paper paper){
		return this.paperDao.updatePaper(paper);
	}
}
