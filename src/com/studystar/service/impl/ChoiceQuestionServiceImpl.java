package com.studystar.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.studystar.dao.ChoiceQuestionDao;
import com.studystar.domin.ChoiceQuestion;
import com.studystar.service.ChoiceQuestionService;

@Service
@Transactional
public class ChoiceQuestionServiceImpl implements ChoiceQuestionService{
	@Autowired
	private ChoiceQuestionDao choiceQuestionDao;
	
	@Override
	public List<ChoiceQuestion> findChoiceQuestionList(){
		return this.choiceQuestionDao.findChoiceQuestionList();
	}
	
	@Override
	public ChoiceQuestion findChoiceQuestionById(Integer id){
		return this.choiceQuestionDao.findChoiceQuestionById(id);
	}
	
	@Override
	public int addChoiceQuestion(ChoiceQuestion choiceQuestion){
		return this.choiceQuestionDao.addChoiceQuestion(choiceQuestion);
	}
	
	@Override
	public int deleteChoiceQuestion(Integer id){
		return this.choiceQuestionDao.deleteChoiceQuestion(id);
	}
	@Override
	public int updateChoiceQuestion(ChoiceQuestion choiceQuestion){
		return this.choiceQuestionDao.updateChoiceQuestion(choiceQuestion);
	}
}
