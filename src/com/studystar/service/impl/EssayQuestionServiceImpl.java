package com.studystar.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.studystar.dao.EssayQuestionDao;
import com.studystar.domin.EssayQuestion;
import com.studystar.service.EssayQuestionService;

@Service
@Transactional
public class EssayQuestionServiceImpl implements EssayQuestionService{
	@Autowired
	private EssayQuestionDao essayQuestionDao;
	
	@Override
	public List<EssayQuestion> findEssayQuestionList(){
		return this.essayQuestionDao.findEssayQuestionList();
	}
	
	@Override
	public EssayQuestion findEssayQuestionById(Integer id){
		return this.essayQuestionDao.findEssayQuestionById(id);
	}
	
	@Override
	public int addEssayQuestion(EssayQuestion essayQuestion){
		return this.essayQuestionDao.addEssayQuestion(essayQuestion);
	}
	
	@Override
	public int deleteEssayQuestion(Integer id){
		return this.essayQuestionDao.deleteEssayQuestion(id);
	}
	
	@Override
	public int updateEssayQuestion(EssayQuestion essayQuestion){
		return this.essayQuestionDao.updateEssayQuestion(essayQuestion);
	}
	
}
