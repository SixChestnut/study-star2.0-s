package com.studystar.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.studystar.dao.CourseDao;
import com.studystar.domin.Course;
import com.studystar.service.CourseService;

@Service
@Transactional
public class CourseServiceImpl implements CourseService{
	@Autowired
	private CourseDao courseDao;
	
	@Override
	public List<Course> findCourseList(){
		return this.courseDao.findCourseList();
	}
	
	@Override
	public Course findCourseById(Integer id){
		return this.courseDao.findCourseById(id);
	}
	
	@Override
	public List<Course> findCourseByGrade(String grade){
		return this.courseDao.findCourseByGrade(grade);
	}
	
	@Override
	public int addCourse(Course course){
		return this.courseDao.addCourse(course);
	}
	
	@Override
	public int deleteCourse(int id){
		return this.courseDao.deleteCourse(id);	
	}
	
	@Override
	public int updateCourse(Course course){
		return this.courseDao.updateCourse(course);
	}
}
