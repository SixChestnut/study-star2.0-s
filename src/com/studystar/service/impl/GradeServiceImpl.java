package com.studystar.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.studystar.dao.GradeDao;
import com.studystar.domin.Grade;
import com.studystar.service.GradeService;

@Service
@Transactional
public class GradeServiceImpl implements GradeService{
	@Autowired
	private GradeDao gradeDao;
	
	@Override
	public List<Grade> findGradeList(){
		return this.gradeDao.findGradeList();
	}
	
	@Override
	public Grade findGradeById(Integer id){
		return this.gradeDao.findGradeById(id);
	}
	
	@Override
	public int addGrade(Grade grade){
		return this.gradeDao.addGrade(grade);
	}

	@Override
	public int deleteGrade(int id){
		return this.gradeDao.deleteGrade(id);
	}
	@Override 
	public int updateGrade(Grade grade){
		return this.gradeDao.updateGrade(grade);
	}
}
