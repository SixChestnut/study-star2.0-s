package com.studystar.service;

import java.util.List;

import com.studystar.domin.Course;

public interface CourseService {
	public List<Course> findCourseList();
	public Course findCourseById(Integer id);
	public List<Course> findCourseByGrade(String grade);
	public int addCourse(Course course);
	public int deleteCourse(int id);
	public int updateCourse(Course course);
}
