package com.studystar.service;

import java.util.List;

import com.studystar.domin.Grade;

public interface GradeService {
	public List<Grade> findGradeList();
	public Grade findGradeById(Integer id);
	public int addGrade(Grade grade);
	public int deleteGrade(int id);
	public int updateGrade(Grade grade);
}
