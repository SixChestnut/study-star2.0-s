package com.studystar.service;

import java.util.List;

import com.studystar.domin.Notice;

public interface NoticeService {
	public List<Notice> findNoticeList();
	public Notice findNoticeById(Integer id);
	public int addNotice(Notice notice);
	public int deleteNotice(int id);
	public int updateNotice(Notice notice);
}
