package com.studystar.service;

import java.util.List;

import com.studystar.domin.QuestionType;

public interface QuestionTypeService {
	public List<QuestionType> findQuestionTypeList();
	public QuestionType findQuestionTypeById(int id);
	public int addQuestionType(QuestionType questionType);
	public int deleteQuestionType(int id);
	public int updateQuestionType(QuestionType questionType);
}
